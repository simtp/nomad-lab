job "app-registry" {
    datacenters = ["site1"]
    group "app-registry" {
        count = 1
        network {
            mode = "bridge"
        }
        service {
            name = "app-registry"
            port = "8080"

            connect {
                sidecar_service {}
            }
        }

        task "app-registry" {
            driver = "docker"
            config {
                image = "simtp/app-registry:latest"
            }
        }
    }
}
