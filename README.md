# Vagrant Lab for SimTP 
In order to successfully use this vagrant configuration you will need to install the following plugins

If using HyperV, then make sure HyperV is enabled in Windows Features and also within PowerShell
```
Enable-WindowsOptionalFeature -Online -FeatureName  Microsoft-Hyper-V-Management-PowerShell
```

```
vagrant plugin install vagrant-hosts
```

Run vagrant with the relevant provider
```bash
vagrant up --provider=virtualbox
vagrant up --provider=hyperv
```

It would be nice to be able to use zeroconf, however consul (and probably nomad) doe not yet support mDNS
````
https://github.com/hashicorp/consul/issues/6666
````


